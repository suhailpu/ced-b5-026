// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;
contract Aadhaar {
 event addedAadhaar(uint c);
 struct aadhaarInfo
{
 bytes32 Name;
 bytes32 Address;
 uint PhoneNumber;
 bytes32 Email ;
 uint BirthDate;
 bool doesExist;
 }
 uint numAadhaar;
 mapping (uint => AadhaarCandidate) AadhaarInfo;
 function addAadhaar(bytes32 name, bytes32 party) public
{
 uint aadhaarID = numAadhaar++;
 aadhaarInfo[aadhaarID] = aadhaarInfo (name, Address,
PhoneNumber, Email, BirthDate, true);
 addedAadhaar(aadhaarID);
}
function getNumOfIndividuals() public view returns(int)
{
 return numAadhaar;
}
function getAadhaar(uint aadhaarID) public view returns
(bytes32,bytes32,uint,bytes32,uint)
{ return(aadhaarID,
aadhaarInfo[aadhaarID].name,
aadhaarInfo[aadhaarID].Address,
aadhaarInfo[aadhaarID].PhoneNumber,
aadhaarInfo[aadhaarID].Email,
aadhaarInfo[aadhaarID].BirthDate);
}
} 